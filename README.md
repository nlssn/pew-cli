# pew-cli

A CLI tool to quickly get started with a new web project

## Note

**pew-cli** was mainly created for my own personal use. If you find a use for it, that's cool too, I guess.

## Available starters

- [pew-11ty](https://gitlab.com/nlssn/pew-11ty)
- [pew-express-api](https://gitlab.com/nlssn/pew-express-api)
- [pew-wp-theme](https://gitlab.com/nlssn/pew-wp-theme)

## How to use

To use **pew-cli**, you need to install it globally via npm, like so:

```
npm i pew-cli -g
```

Then run `pew` to start a new project.

## Todo

- [ ] Add more starters
